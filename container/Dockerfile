FROM index.docker.io/ubuntu:jammy

ARG GIT_CORE_PPA_KEY="A1715D88E1DF1F24"
ARG GIT_LFS_VERSION="3.2.0"
ARG GH_RUNNER_VERSION="2.304.0"
ARG TARGETPLATFORM

ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

ENV DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=DL3008
RUN apt-get update -y \
  && apt-get install -y --no-install-recommends apt-utils locales gnupg \
  && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys "${GIT_CORE_PPA_KEY}" \
  && apt-get update -y \
  && apt-get install -y --no-install-recommends \
    lsb-release \
    curl \
    tar \
    unzip \
    zip \
    apt-transport-https \
    ca-certificates \
    sudo \
    gpg-agent \
    software-properties-common \
    build-essential \
    zlib1g-dev \
    libghc-zlib-dev \
    zstd \
    gettext \
    libcurl4-openssl-dev \
    inetutils-ping \
    jq \
    wget \
    dirmngr \
    openssh-client \
    locales \
    python3-pip \
    python3-setuptools \
    python3 \
    dumb-init \
    nodejs \
    rsync \
    gosu \
  && DPKG_ARCH="$(dpkg --print-architecture)" \
  && LSB_RELEASE_CODENAME="$(lsb_release -cs)" \
  && sed -e 's/Defaults.*env_reset/Defaults env_keep = "HTTP_PROXY HTTPS_PROXY NO_PROXY FTP_PROXY http_proxy https_proxy no_proxy ftp_proxy"/' -i /etc/sudoers \
  && apt-get update -y \
  && ( apt-get install -y --no-install-recommends git || apt-get install -t stable -y --no-install-recommends git || : ) \
  && ( apt-get install -y --no-install-recommends liblttng-ust1 ) \
  && ( curl "https://awscli.amazonaws.com/awscli-exe-linux-$(uname -m).zip" -o "awscliv2.zip" && unzip -q awscliv2.zip -d /tmp/ && /tmp/aws/install && rm -v awscliv2.zip && rm -rf /tmp/aws && aws --version ) \
  && ( curl -L "https://github.com/git-lfs/git-lfs/releases/download/v${GIT_LFS_VERSION}/git-lfs-linux-${DPKG_ARCH}-v${GIT_LFS_VERSION}.tar.gz" -o /tmp/lfs.tar.gz && tar -xzf /tmp/lfs.tar.gz -C /tmp && /tmp/git-lfs-"${GIT_LFS_VERSION}"/install.sh && rm -rf /tmp/lfs.tar.gz /tmp/git-lfs-"${GIT_LFS_VERSION}" ) \
  && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
  && echo "deb [arch=${DPKG_ARCH} signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu ${LSB_RELEASE_CODENAME} stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
  && apt-get update -y \
  && apt-get install -y docker-ce docker-ce-cli docker-buildx-plugin containerd.io docker-compose-plugin --no-install-recommends \
  && echo -e '#!/bin/sh\ndocker compose --compatibility "$@"' > /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose \
  && GH_CLI_VERSION=$(curl -sL -H "Accept: application/vnd.github+json" https://api.github.com/repos/cli/cli/releases/latest | jq -r '.tag_name' | sed -e 's|^v||g') \
  && GH_CLI_DOWNLOAD_URL=$(curl -sL -H "Accept: application/vnd.github+json" https://api.github.com/repos/cli/cli/releases/latest | jq ".assets[] | select(.name == \"gh_${GH_CLI_VERSION}_linux_${DPKG_ARCH}.deb\")" | jq -r '.browser_download_url') \
  && curl -sSLo /tmp/ghcli.deb "${GH_CLI_DOWNLOAD_URL}" && apt-get -y install /tmp/ghcli.deb --no-install-recommends && rm /tmp/ghcli.deb \
  && ( rm -rf /var/lib/apt/lists/* || : ) \
  && ( rm -rf /tmp/* || : ) \
  && groupadd -g 121 runner \
  && useradd -mr -d /home/runner -u 1001 -g 121 runner \
  && usermod -aG sudo runner \
  && usermod -aG docker runner \
  && echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

ENV AGENT_TOOLSDIRECTORY=/opt/hostedtoolcache
RUN mkdir -p /opt/hostedtoolcache

WORKDIR /actions-runner
COPY install_actions.sh /actions-runner

RUN chmod +x /actions-runner/install_actions.sh \
  && /actions-runner/install_actions.sh ${GH_RUNNER_VERSION} ${TARGETPLATFORM} \
  && rm /actions-runner/install_actions.sh \
  && chown runner /_work /actions-runner /opt/hostedtoolcache

COPY token.sh entrypoint.sh app_token.sh start.sh /
RUN chmod +x /token.sh /entrypoint.sh /app_token.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD [ "/bin/bash", "/start.sh" ]
